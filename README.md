# Описание программы #

## Краткое описание ##

Утилита для получения полного наименования сокращения и его описания.

## Принцип работы ##

Используется таблица, хранящаяся в репозитории Gitlab:
`https://gitlab.com/common_info/annex/-/blob/main/terms.md`

## Правила ввода ##

Искомые термины и аббревиатуры можно вводить как по одному, так и несколько.<br/>
При вводе нескольких необходимо отделить друг от друга любым не циферным и не буквенным символом, кроме '\_' и '-'.<br/>
Ввод не чувствителен к регистру символов.

## Вывод ответа на запрос ##

Если ввод корректный, и термины известны, то для каждого выводится:

* сокращение на английском языке;
* расшифровка сокращения на английском языке (при наличии);
* расшифровка сокращения на русском языке (при наличии);
* комментарий на русском языке (при наличии).

### Формат вывода ###

```
<short eng term>
<full eng term>, <full rus term> — <description rus>
```

### Пример вывода ###

```
BFCP
Binary Floor Control Protocol, протокол управления на двоичном уровне — протокол для обмена презентациями во время видеоконференций
```

## Специальные запросы ##

В программе предусмотрены особые запросы для получения информации о словаре:

* `__all__` --- вывод всех терминов и сокращений на английском;
* `__full__` --- вывод всех терминов и сокращений, а также их полные расшифровки и комментарии;
* `__help__`, `-h`, `--help` --- вывод краткой справочной информации;
* `__readme__` --- вывод полной справочной информации из файла `README.md`.

## Логирование ##

Журнал выполнения программы записывается в директорию<br/>
`~HOME > Desktop > terms_logs > terms_debug.log`.

По умолчанию, директория удаляется, если работа программы завершилась в штатном режиме. При возникновении ошибки 
директория остается, и в дальнейшем будет использоваться для исправления некорректной обработки сценария.

Для сохранения директории и лог-файла необходимо добавить переменную окружения `KEEP_TERMS_LOGS` с любым значением.

## Выход из программы ##

Для выхода необходимо ввести любой из нижепредложенных вариантов (без кавычек):
* `__exit__`
* `exit`
* `__quit__`
* `quit`
* `!q`
* `!quit`
* `!e`
* `!exit`

# Пример работы #

## Запуск программы ##

```
C:\Users\...\venv\venv\Scripts\python.exe

В случае ошибки рекомендуется сохранить лог-файл, который будет находиться здесь:
'./logs/terms_debug.log'

https://gitlab.com/common_info/terms/-/tree/main

Утилита для получения полного наименования сокращения и его описания.
Используется таблица, хранящаяся в репозитории Gitlab:
https://gitlab.com/common_info/annex/-/blob/main/terms.md
Искомые термины и аббревиатуры можно вводить как по одному, так и несколько.
При вводе нескольких необходимо отделить друг от друга любым не циферным и не буквенным символом, кроме '_' и '-'.

Для вывода информации о списке необходимо ввести:
__all__ --- отобразить все термины и сокращения на английском;
__full__ --- вывод всех терминов и сокращений, а также их полные расшифровки и комментарии;

Для вывода краткой справочной информации необходимо ввести любой из нижепредложенных вариантов:
__help__, -h, --help
Для вывода полной справочной информации README.md необходимо ввести:
__readme__

Для выхода необходимо ввести любой из нижепредложенных вариантов:
__exit__, exit, __quit__, quit, !e, !exit, !q, !quit
```

## Строка для ввода значений ##

```
Введите сокращение или несколько сокращений, чтобы получить полное наименование и описание:
```

## Сокращение с одним значением ##

Вывод: **Значение сокращения**

```
Введите сокращение или несколько сокращений, чтобы получить полное наименование и описание:
PC
PC
Protocol Class, класс протокола
--------------------
```

## Сокращение с несколькими значениями ##

Вывод: **Все значения сокращения**

```
Введите сокращение или несколько сокращений, чтобы получить полное наименование и описание:
LAC
LAC
L2TP Access Concentrator, концентратор доступа L2TP
LAC
Local Area Code, код локальной зоны
--------------------
```

## Неизвестное сокращение ##

Вывод: **Не найдено**

```
Введите сокращение или несколько сокращений, чтобы получить полное наименование и описание:
QWE
Не найдено
--------------------
```

## Ввод нескольких известных сокращений ##

Вывод: **Все значения известных сокращений**

```
Введите сокращение или несколько сокращений, чтобы получить полное наименование и описание:
SCP SRF;DPC/ASP
SCP
Service Control Point, модуль логик услуг, реализованных посредством протокола CAMEL
SRF
Signaling Relay Function, функция ретрансляции сигнальных сообщений
DPC
Destination Point Code, код сигнальной точки назначения
ASP
Application Server Process, отдельный экземпляр AS SIGTRAN
ASP
Application Service Provider, поставщик услуг доступа к приложениям
--------------------
```

## Ввод нескольких корректных сокращений, среди которых есть неизвестные ##

Вывод: **Все значения известных сокращений, все неизвестные пропускаются**

```
Введите сокращение или несколько сокращений, чтобы получить полное наименование и описание:
LCS LI LOS
LCS
Location Service, служба определения местоположения
LI
Legal Intervention, законное вмешательство в вызов
LI
Length Indicator, индикатор длины
LI
Log Intelligence, обработка лог-файлов
--------------------
```

## Некорректный ввод ##

Вывод: **Не найдено**

```
Введите сокращение или несколько сокращений, чтобы получить полное наименование и описание:
~A
Не найдено
--------------------
```

## Выход ##

```
Введите сокращение или несколько сокращений, чтобы получить полное наименование и описание:
__exit__
Нажмите любую клавишу, чтобы закрыть окно ...
```
