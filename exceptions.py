from requests import HTTPError


class BinarySearchFailed(KeyError):
    """Binary search failed to find the term."""


class HTTPRequestFailed(HTTPError):
    """HTTP request failed to get the dictionary."""


class HTTPRequestResultFailed(HTTPError):
    """HTTP request is successful but response has no awaiting content."""


class KeyFormatFailed(TypeError):
    """Term dictionary key has invalid format."""


class ImportLibraryDependencyFailed(ImportError):
    """Dependency lib failed to import."""
