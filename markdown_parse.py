import re
from re import Match, Pattern
from typing import NamedTuple


class _Term(NamedTuple):
    short: str = None
    full: str = None
    rus: str = None
    commentary: str = None

    def __repr__(self):
        return f"<{self.__class__.__name__}(short={self.short}, full={self.full}, rus={self.rus}, " \
               f"commentary={self.commentary})>"

    def __str__(self):
        return f"{self.short}\n{self.full}, {self.rus} -- {self.commentary}"

    def _multiplier(self, attr: str) -> int:
        value: str = getattr(self, attr)
        return int(value != "")

    @property
    def is_empty(self):
        for v in self._asdict().values():
            if v:
                return False
        return True

    @property
    def _type_table(self) -> dict[str, int]:
        return {
            "full": 1,
            "rus": 2,
            "commentary": 4
        }

    def _formatting_type(self) -> int:
        """1 * bool(full) + 2 * bool(rus) + 4 * bool(commentary)"""
        return sum(map(lambda x: self._multiplier(x) * self._type_table[x], self._type_table.keys()))

    def _formatting_description(self) -> str:
        match self._formatting_type():
            case 1:
                return f"{self.full}"
            case 2:
                return f"{self.rus}"
            case 3:
                return f"{self.full}, {self.rus}"
            case 4:
                return f"{self.commentary}"
            case 5:
                return f"{self.full} — {self.commentary}"
            case 6:
                return f"{self.rus} — {self.commentary}"
            case 7:
                return f"{self.full}, {self.rus} — {self.commentary}"
            case _:
                return ""

    def formatted(self) -> str:
        if self.is_empty:
            return "Не найдено"
        return f"{self.short}\n{self._formatting_description()}"


class MarkdownTableParser:
    _instance = None
    _table: list[str] = None
    _label = -1
    _headers = None
    start: int = None
    pattern: Pattern = re.compile(r"\|\s(.+)\s\|\s(.*)\s\|\s(.*)\s\|\s(.*)\s\|")

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    @classmethod
    def __call__(cls, *args, **kwargs):
        return cls._instance

    @classmethod
    def operate(cls, table: list[str]):
        cls._set_table(table)
        cls._get_table_label()
        cls._get_table_header()
        cls.start = cls._headers[1] + 1

    @classmethod
    def _set_table(cls, table: list[str]):
        cls._table = table

    @classmethod
    def _get_table_label(cls):
        for index, line in enumerate(cls._table):
            if line.startswith("#"):
                cls._label = index
                return

    @classmethod
    def _get_table_header(cls):
        for index, line in enumerate(cls._table):
            if line.startswith("| :") and line.endswith("- |"):
                cls._headers = (index - 1, index)
                return

    @classmethod
    def _get_term_attributes(cls, line) -> list[str]:
        match: Match = re.match(cls.pattern, line)
        return [match.group(i) for i in range(1, 5)]

    @classmethod
    def handle_term_attributes(cls) -> list[_Term]:
        terms: list[_Term] = []
        for line in cls._table[cls.start:-1]:
            _term_attributes: list[str] = cls._get_term_attributes(line)
            if _term_attributes:
                short, full, rus, commentary = _term_attributes
                term = _Term(short, full, rus, commentary)
            else:
                term = _Term()
            terms.append(term)
        return terms
