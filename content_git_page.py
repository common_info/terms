import base64

import requests
from loguru import logger
from requests import Response, HTTPError

from exceptions import HTTPRequestFailed, HTTPRequestResultFailed


class ContentGitPage:
    __slots__ = ("method", "url", "params")
    _instance = None
    _response_json: dict[str, str] = None

    def __new__(
            cls,
            method: str = None,
            url: str = None,
            params: dict[str, str] = None,
            *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)

            if method is None:
                method: str = "GET"
            if url is None:
                url: str = "https://gitlab.com/api/v4/projects/common_info%2Fannex/repository/files/terms.md"
            if params is None:
                params: dict[str, str] = {"ref": "main"}

            cls.method: str = method
            cls.url: str = url
            cls.params: dict[str, str] = params
        return cls._instance

    @classmethod
    def get_git_page(cls):
        try:
            response: Response = requests.request(method=cls.method, url=cls.url, params=cls.params)
            response.raise_for_status()
        except HTTPError as e:
            logger.error(f"{e.__class__.__name__}, {e.strerror}\nЗапрос: {e.request}\nОтвет:{e.response}")
            raise HTTPRequestFailed
        else:
            if response.status_code != 200:
                logger.error(f"HTTP-код статуса {response.status_code}")
                raise HTTPRequestResultFailed
            cls._response_json = response.json()

    def _readable_content(self) -> str:
        base64_content: str = self._response_json.get("content")
        return base64.b64decode(base64_content).decode("utf-8")

    def content(self) -> list[str]:
        return self._readable_content().split("\n")
