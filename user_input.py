import re
import textwrap
from pathlib import Path
from re import Pattern
from typing import Optional

from loguru import logger

from dictionary import DictionaryTerms
from markdown_parse import _Term


class UserInputParser:
    _help_text: str = textwrap.dedent("""
    --------------------
    Справка\n
    Утилита для получения полного наименования сокращения и его описания.
    Git: https://gitlab.com/common_info/annex/-/blob/main/terms.md
    
    Искомые термины и аббревиатуры можно вводить как по одному, так и несколько.
    При вводе нескольких необходимо отделить друг от друга любым не циферным и не буквенным символом, кроме _ и -.
    Ввод не чувствителен к регистру символов.
    
    Если ввод корректный, и термины известны, то для каждого выводится:
    * сокращение на английском языке;
    * расшифровка сокращения на английском языке (при наличии);
    * расшифровка сокращения на русском языке (при наличии);
    * комментарий на русском языке (при наличии).
    
    BFCP
    Binary Floor Control Protocol, протокол управления на двоичном уровне — протокол для обмена презентациями во время видеоконференций
    
    В программе предусмотрены особые запросы для получения информации о словаре:
    * __all__ --- вывод всех терминов и сокращений на английском;
    * __full__ --- вывод всех терминов и сокращений, а также их полные расшифровки и комментарии;
    * __help__, -h, --help --- вывод краткой справочной информации;
    * __readme__ --- вывод полной справочной информации из файла README.md.
    
    Для выхода необходимо ввести любое из нижепредложенных вариантов:
    __exit__, exit, __quit__, quit, !q, !quit, !e, !exit
    --------------------
    """)
    _interceptors: tuple[str, ...] = ("__exit__", "exit", "__quit__", "quit", "!q", "!quit", "!e", "!exit")
    _help: tuple[str, ...] = ("__help__", "-h", "--help")
    _term_pattern: Pattern = re.compile(r"([\w_-]+)")
    _dictionary_terms: DictionaryTerms = None
    _instance = None
    _separator = "--------------------\n"

    def __new__(cls, dictionary_terms: DictionaryTerms):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
            cls._dictionary_terms: DictionaryTerms = dictionary_terms
        return cls._instance

    @classmethod
    def __call__(cls, *args, **kwargs):
        return cls._instance

    @classmethod
    def _search_terms(cls, user_input: str) -> Optional[list[str]]:
        terms: list[str] = []
        inputs: list[str] = re.findall(cls._term_pattern, user_input.upper())
        if not inputs:
            return
        for item in inputs:
            term = cls._dictionary_terms.__getitem__(item)
            if term is None:
                continue
            elif isinstance(term, str):
                terms.append(term)
            else:
                terms.append(_Term().formatted())
        return terms

    @classmethod
    def _print_terms(cls, user_input: str) -> str:
        found_terms: Optional[list[str]] = cls._search_terms(user_input)
        if found_terms:
            return "\n".join(found_terms)
        return "Не найдено"

    @classmethod
    def _print_dictionary(cls) -> str:
        return "\n".join(list(map(
            lambda x: x.formatted(),
            [value for values in cls._dictionary_terms.dict_terms.values() for value in values])))

    @classmethod
    def _print_short(cls) -> str:
        return ", ".join(cls._dictionary_terms.terms_short())

    @classmethod
    def _print_help(cls) -> str:
        return cls._help_text

    @classmethod
    def _print_readme(cls) -> str:
        with open(Path(__file__).with_name("README.md"), "rb+") as readme_md:
            readme_text: bytes = readme_md.read()
        return readme_text.decode(encoding="utf8")

    @classmethod
    def handle_input(cls, user_input: str = None) -> bool:
        results: Optional[str]
        _input_lower: str = user_input.lower()
        _input_strip: str = user_input.lower().strip()

        if _input_strip in cls._interceptors:
            results = None
        elif _input_strip in cls._help:
            results: Optional[str] = cls._print_help()
        else:
            match _input_lower:
                case "__full__":
                    results: str = "\n".join((cls._print_dictionary(), cls._separator))
                case "__all__":
                    results: str = "\n".join((cls._print_short(), cls._separator))
                case "__readme__":
                    results: str = "\n".join((cls._print_readme(), cls._separator))
                case _:
                    results: str = "\n".join((cls._print_terms(user_input.upper()), cls._separator))
        if results:
            logger.info(results)
        return results is not None
