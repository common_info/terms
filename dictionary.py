from bisect import bisect_left
from typing import MutableMapping, Optional

from loguru import logger

from markdown_parse import _Term, MarkdownTableParser


class DictionaryTerms:
    _instance = None
    _table: list[str] = None
    dict_terms: MutableMapping[str, tuple[_Term, ...]] = dict()

    def __new__(cls, table: list[str]):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
            cls._table = table
        return cls._instance

    @classmethod
    def __getitem__(cls, item):
        if isinstance(item, str):
            terms = cls._binary_search(item)
            if terms:
                return "\n".join([term.formatted() for term in terms])
            else:
                logger.debug(f"Термин {item} не найден")
        else:
            return NotImplemented

    @classmethod
    def __call__(cls, *args, **kwargs):
        return cls._instance

    @classmethod
    def len(cls) -> int:
        return len(cls._table)

    @classmethod
    def get_terms(cls):
        terms: list[_Term] = MarkdownTableParser().handle_term_attributes()
        _dict_proxy: dict[str, list[_Term]] = dict()
        for term in terms:
            term_short: str = term.short
            if term_short not in _dict_proxy:
                _dict_proxy[term_short] = []
            _dict_proxy[term_short].append(term)
        cls.dict_terms = {k.upper(): (*v,) for k, v in _dict_proxy.items()}
        return

    @classmethod
    def _index_to_key(cls, index: Optional[int]):
        return cls.terms_short().__getitem__(index) if index else None

    @classmethod
    def terms_short(cls) -> tuple[str, ...]:
        return tuple(map(lambda x: x.upper(), cls.dict_terms.keys()))

    @classmethod
    def _binary_search(
            cls,
            search_value: str,
            min_index: int = 0,
            max_index: int = None) -> Optional[tuple[_Term, ...]]:
        """
        Specifies the binary search to accelerate the common one.

        :param str search_value: the value to search
        :param int min_index: the lower bound, default = 0
        :param max_index: the higher bound, default = len(values)
        :return: the searched value position in the list if the value is in the list.
        :rtype: int
        """
        if max_index is None:
            max_index = cls.len()
        _index: int = bisect_left(cls.terms_short(), search_value.upper(), min_index, max_index)
        _term_short: str = cls._index_to_key(_index)
        if _index != max_index and _term_short == search_value:
            return cls.dict_terms.get(_term_short)
        return
