import logging
import time
from logging import LogRecord, Handler
from os import environ
from pathlib import Path
from textwrap import dedent
from types import FrameType
from typing import Any

from loguru import logger

from dictionary import DictionaryTerms
from markdown_parse import MarkdownTableParser
from content_git_page import ContentGitPage
from init_logger import LoggerConfiguration, HandlerType, LoggingLevel
from user_input import UserInputParser


def _check_delete_logs():
    try:
        value = environ.get("KEEP_TERMS_LOGS")
    except KeyError:
        _delete_logs()
    except OSError as e:
        logger.error(f"{e.__class__.__name__}, {e.strerror}")
        raise
    else:
        if bool(value):
            _delete_logs()


def _delete_logs():
    logger.stop()
    log_folder: Path = Path.home().joinpath("Desktop").joinpath("terms_logs").resolve()
    for file in log_folder.iterdir():
        file.unlink()
    log_folder.rmdir()


def convert_ns_to_readable(time_ns: int) -> str:
    hours: int
    minutes: int
    seconds: int = time_ns // 10 ** 9
    if seconds == 0:
        return "менее секунды"
    if seconds < 60:
        hours = 0
        minutes = 0
    else:
        hours = 0
        minutes = seconds // 60
        seconds = seconds % 60
        if minutes >= 60:
            hours = minutes // 60
            minutes = minutes % 60
    hour_string: str = f"{hours} ч " if hours != 0 else ""
    minutes_string: str = f"{minutes} мин " if minutes != 0 else ""
    seconds_string: str = f"{seconds} с" if seconds != 0 else ""
    return f"{hour_string}{minutes_string}{seconds_string}"


def _disclaimer():
    log_file: Path = Path.home().joinpath("Desktop").joinpath("terms_logs").joinpath("terms_debug.log").resolve()
    return dedent(f"""
    В случае ошибки необходимо сохранить лог-файл, который будет находиться здесь:
    '{log_file}'
    
    https://gitlab.com/common_info/terms/-/tree/main
    
    Утилита для получения полного наименования сокращения и его описания.
    Используется таблица, хранящаяся в репозитории Gitlab:
    https://gitlab.com/common_info/annex/-/blob/main/terms.md
    Искомые термины и аббревиатуры можно вводить как по одному, так и несколько.
    При вводе нескольких необходимо отделить друг от друга любым не циферным и не буквенным символом, кроме '_' и '-'.
    """)


def _spec():
    return dedent("""\
    Для вывода информации о списке необходимо ввести:
    __all__ --- отобразить все термины и сокращения на английском;
    __full__ --- вывод всех терминов и сокращений, а также их полные расшифровки и комментарии;
    """)


def _help():
    return dedent("""\
    Для вывода краткой справочной информации необходимо ввести любой из нижепредложенных вариантов:
    __help__, -h, --help
    Для вывода полной справочной информации README.md необходимо ввести:
    __readme__
    """)


def _exit():
    return dedent("""\
    Для выхода необходимо ввести любой из нижепредложенных вариантов (без кавычек):
    __exit__, exit, __quit__, quit, !e, !exit, !q, !quit 
    """)


class InterceptHandler(Handler):
    def emit(self, record: LogRecord):
        # Get corresponding loguru level if it exists
        try:
            level: str = logger.level(record.levelname).name
        except ValueError:
            level: int = record.levelno
        # Find caller from where originated the logged message
        frame, depth = logging.currentframe(), 2
        while frame.f_code.co_filename == logging.__file__:
            frame: FrameType = frame.f_back
            depth += 1
        logger.opt(depth=depth, exception=record.exc_info).log(level, record.getMessage())


def main():
    _interceptors = ("__exit__", "exit", "__quit__", "quit", "!q", "!quit", "!e", "!exit")

    content_git_page: ContentGitPage = ContentGitPage()
    content_git_page.get_git_page()
    table: list[str] = content_git_page.content()

    markdown_table_parser: MarkdownTableParser = MarkdownTableParser()
    markdown_table_parser.operate(table)

    table_dictionary: list[str] = table[markdown_table_parser.start:-1]
    dictionary_terms_git: DictionaryTerms = DictionaryTerms(table_dictionary)
    dictionary_terms_git.get_terms()

    user_input_parser: UserInputParser = UserInputParser(dictionary_terms_git)

    prompt: str = "Введите сокращение или несколько сокращений, чтобы получить полное наименование и описание:\n"

    logger.info(_disclaimer())
    logger.info(_spec())
    logger.info(_help())
    logger.info(_exit())

    __stop_flag: bool = True
    while __stop_flag:
        user_input: str = input(prompt)
        logger.debug(user_input)
        __stop_flag = user_input_parser.handle_input(user_input)

    input("Нажмите любую клавишу, чтобы закрыть окно ...")


if __name__ == "__main__":
    start_time: int = time.perf_counter_ns()
    handlers: dict[HandlerType, LoggingLevel] = {
        "stream": "INFO",
        "file_rotating": "DEBUG"
    }

    file_name: str = "terms"

    logger_configuration: LoggerConfiguration = LoggerConfiguration(file_name, handlers)
    stream_handler: dict[str, Any] = logger_configuration.stream_handler()
    rotating_file_handler: dict[str, Any] = logger_configuration.rotating_file_handler()

    logger.configure(handlers=[stream_handler, rotating_file_handler])

    logging.basicConfig(handlers=[InterceptHandler()], level=0)
    logger.debug("========================================")
    logger.debug("Запуск программы:")

    main()

    end_time: int = time.perf_counter_ns()
    delta_time: int = end_time - start_time
    logger.debug(f"Время выполнения: {delta_time} наносекунд, {convert_ns_to_readable(delta_time)}\n")

    _check_delete_logs()
