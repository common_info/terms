from functools import cached_property
from pathlib import Path
from sys import stdout as sysout
from typing import Type, Literal, Optional, Any

__all__ = ["LoggerConfiguration", "HandlerType", "LoggingLevel"]


HandlerType: Type[str] = Literal["stream", "file_rotating"]
LoggingLevel: Type[str] = Literal["TRACE", "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]


class LoggerConfiguration:
    _log_folder: Path = Path.home().joinpath("Desktop").joinpath("terms_logs").resolve()

    def __init__(
            self,
            file_name: str,
            handlers: dict[HandlerType, LoggingLevel] = None):
        if handlers is None:
            handlers = dict()
        self._file_name: str = file_name
        self._handlers: dict[str, str] = handlers

    @staticmethod
    def _no_http(record: dict):
        return not record["name"].startswith("urllib3")

    @cached_property
    def _colored_format(self):
        return " | ".join(
            ("<green>{time:DD-MMM-YYYY HH:mm:ss}</green>::<level>{level.name}</level>",
             "<cyan>{module}</cyan>::<cyan>{function}</cyan>",
             "<cyan>{file.name}</cyan>::<cyan>{name}</cyan>::<cyan>{line}</cyan>",
             "\n<level>{message}</level>")
        )

    @cached_property
    def _wb_format(self):
        return " | ".join(
            ("{time:DD-MMM-YYYY HH:mm:ss}::{level.name}",
             "{module}::{function}",
             "{file.name}::{name}::{line}",
             "\n{message}")
        )

    @cached_property
    def _user_format(self):
        return "{message}"

    def stream_handler(self):
        try:
            _logging_level: Optional[str] = self._handlers.get("stream")
            _handler: dict[str, Any] = {
                "sink": sysout,
                "level": _logging_level,
                "format": self._user_format,
                "colorize": True,
                "filter": self._no_http,
                "diagnose": True
            }
        except KeyError:
            return
        else:
            return _handler

    def rotating_file_handler(self):
        try:
            _log_path: str = f"{self._log_folder}/{self._file_name}_debug.log"
            _logging_level: str = self._handlers.get("file_rotating")
            _handler: dict[str, Any] = {
                "sink": f"{Path(__file__).resolve().parent.joinpath(_log_path)}",
                "level": _logging_level,
                "format": self._wb_format,
                "colorize": False,
                "diagnose": True,
                "rotation": "2 MB",
                "mode": "a",
                "encoding": "utf8"
            }
        except KeyError:
            return
        else:
            return _handler
