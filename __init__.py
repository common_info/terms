import importlib
import subprocess
import warnings
from pathlib import Path
from sys import executable

from exceptions import ImportLibraryDependencyFailed


_parent_path: Path = Path(__file__).resolve().parent
__path__.append(_parent_path)


warnings.simplefilter("ignore")


def install_package(package: str):
    try:
        subprocess.run([executable, "-m", "pip", "install", package]).check_returncode()
    except subprocess.CalledProcessError as error:
        print(f"{error.__class__.__name__}\nКод ответа {error.returncode}\nОшибка {error.output}")
        print(f"Не удалось импортировать пакет `{package}`.")
        raise ImportLibraryDependencyFailed
    except OSError as error:
        print(f"{error.__class__.__name__}\nФайл {error.filename}\nОшибка {error.strerror}")
        print(f"Не удалось импортировать пакет `{package}`")
        raise ImportLibraryDependencyFailed
    else:
        print(f"Установлен пакет {package}")


_packages: tuple[str, ...] = ("requests", "loguru")
for _package in _packages:
    try:
        importlib.import_module(_package)
    except ModuleNotFoundError:
        install_package(_package)
    except ImportError:
        install_package(_package)

_log_folder: Path = _parent_path.joinpath("logs")

try:
    _log_folder.resolve(strict=True)
except RuntimeError as e:
    print(f"{e.__class__.__name__}\n{str(e)}")
    raise
except FileNotFoundError:
    _log_folder.touch(exist_ok=True)
